# -*-coding: utf-8-*-

# Author     : Paul
# FileName   : setup.py
# DateTime   : 2019/11/27 17:56
# Description:

from setuptools import setup, find_packages

setup(
    name="twinkle_algo_plat",
    version="1.0.0",
    author="Paul",
    packages=find_packages(),

    include_package_data=True,

    # 需要安装的依赖
    install_requires=[
        # 'PyHive>=0.6.4',
        # 'PyMysql>=1.0.2',
        # 'hdfs>=2.6.0',
        # 'psycopg2>=2.9.1'
        # 'requests>=2.26.0'
    ]

)
