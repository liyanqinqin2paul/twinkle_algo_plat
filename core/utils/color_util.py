# _*_ coding: utf-8 _*_
# @Date : 2023/3/11 21:02
# @Author : Paul
# @File : color_util.py
# @Description :
import numpy as np

class ColorUtil():

    @staticmethod
    def getRandomColor():
        return np.random.rand(1, 3)

if __name__ == '__main__':
    for i in range(10):
        print(ColorUtil.getRandomColor())