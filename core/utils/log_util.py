# _*_ coding: utf-8 _*_
# @Date : 2023/3/13 18:29
# @Author : Paul
# @File : log_util.py
# @Description :

class LogUtil:

    @staticmethod
    def saveClusterResult(meta_data_source, algoResult):
        meta_data_source.insert("algo_clster_result", algoResult.data)

    @staticmethod
    def saveClassifierResult(meta_data_source, classifierResult):
        meta_data_source.insert("algo_classifier_result", classifierResult.data)

    @staticmethod
    def saveRegressionResult(meta_data_source, classifierResult):
        meta_data_source.insert("algo_regression_result", classifierResult.data)

    @staticmethod
    def savePreViewDataResult(meta_data_source, previewDataResult):
        meta_data_source.insert("algo_preview_data_result", previewDataResult.data)

    @staticmethod
    def saveParamTrainResult(meta_data_source, paramTrainResult):
        meta_data_source.insert("algo_param_train_result", paramTrainResult.data)