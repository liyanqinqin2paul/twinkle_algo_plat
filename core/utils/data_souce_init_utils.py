# _*_ coding: utf-8 _*_
# @Date : 2023/3/11 17:14
# @Author : Paul
# @File : data_souce_init_utils.py
# @Description :
from core.data_source.myql.mysql_common_source import MysqlCommonDataSource

class DataSourceInitUtil():

    @staticmethod
    def getDataBase(meta_data_source, data_source_id):
        data_base_info = meta_data_source.get_data_base(data_source_id)
        if data_base_info["type"] == "mysql":
            return MysqlCommonDataSource(data_base_info["ip"],
                                         int(data_base_info["port"]),
                                         data_base_info["user"],
                                         data_base_info["password"],
                                         data_base_info["data_base"])

