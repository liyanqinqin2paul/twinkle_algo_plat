# _*_ coding: utf-8 _*_
# @Date : 2023/3/11 20:49
# @Author : Paul
# @File : date_util.py
# @Description : 日期工具类
import time
import datetime

class DateUtil():

    @staticmethod
    def getCurrentDateSimple():
        return time.strftime("%Y%m%d%H%M%S", time.localtime(time.time()))

    @staticmethod
    def getCurrentDate():
        return time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(time.time()))

    @staticmethod
    def getCurrentDateFormat(format):
        return time.strftime(format, time.localtime(time.time()))

    @staticmethod
    def diffMin(startTime, endTime):
        '''计算两个时间点之间的分钟数'''
        # 处理格式,加上秒位
        # 计算分钟数
        startTime2 = datetime.datetime.strptime(startTime, "%Y-%m-%d %H:%M:%S")
        endTime2 = datetime.datetime.strptime(endTime, "%Y-%m-%d %H:%M:%S")
        seconds = (endTime2 - startTime2).seconds
        # 来获取时间差中的秒数。注意，seconds获得的秒只是时间差中的小时、分钟和秒部分的和，并没有包含时间差的天数（既是两个时间点不是同一天，失效）
        total_seconds = (endTime2 - startTime2).total_seconds()
        return int(total_seconds)



if __name__ == '__main__':
    print(DateUtil.getCurrentDate())
    print(DateUtil.getCurrentDateFormat("%Y-%m-%d %H:%M:%S"))

