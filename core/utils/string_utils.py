# _*_ coding: utf-8 _*_
# @Date : 2023/3/20 20:48
# @Author : Paul
# @File : string_utils.py
# @Description :
import stringutils

class StringUtils():

    @staticmethod
    def isBlack(str):
        if str == None:
            return True
        return stringutils.is_whitespace(str)

if __name__ == '__main__':
    print(StringUtils.isBlack("aa"))