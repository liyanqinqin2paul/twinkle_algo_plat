# _*_ coding: utf-8 _*_
# @Date : 2023/4/6 21:48
# @Author : Paul
# @File : config_twinklel.py
# @Description :

class ConfigTwinkle():

    metabase_mysql_host = "127.0.0.1"
    metabase_mysql_port = 3306
    metabase_mysql_user_name = "root"
    metabase_mysql_pass_word = "root"
    metabase_mysql_data_base = "twinkle"