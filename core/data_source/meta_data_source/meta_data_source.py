# _*_ coding: utf-8 _*_
# @Date : 2023/3/11 10:05
# @Author : Paul
# @File : meta_data_source.py
# @Description :
from core.config.config_twinklel import ConfigTwinkle
from core.data_source.myql.mysql_common_source import MysqlCommonDataSource
import pandas as pd

class MetaDataSource():

    def __init__(self):
        config_twinkle = ConfigTwinkle()
        self.meta_data_source = MysqlCommonDataSource(config_twinkle.metabase_mysql_host,
                                                      config_twinkle.metabase_mysql_port,
                                                      config_twinkle.metabase_mysql_user_name,
                                                      config_twinkle.metabase_mysql_pass_word,
                                                      config_twinkle.metabase_mysql_data_base
                                                      )
    def get_data_base(self, data_base_id):
        sql = "select * from dw_data_base where id={}".format(data_base_id)
        return self.meta_data_source.queryOrderedDict(sql)[0]

    def insert(self, table_name, data_dict):
        return self.meta_data_source.insert(table_name, data_dict)
