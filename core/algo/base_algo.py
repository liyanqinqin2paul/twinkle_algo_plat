# _*_ coding: utf-8 _*_
# @Date : 2023/3/11 9:53
# @Author : Paul
# @File : base_algo.py
# @Description : 算法基础类
import matplotlib.pyplot as plt

class BaseAlgo():

    def __init__(self, app_name="base_algo"):
        """
        初始化方法
        """
        self.IS_APPEND_MODEL = False    #默认：非增量训练模型
        self.IS_LOAD_MODEL = False      #默认：不需要加载模型
        self.IS_MODEL_EVAL = False      #默认：不需要评估模型
        self.IS_SAVE_MODEL = False      #默认：不需要保存模型
        self.IS_TRAIN_MODEL = True      # 默认：需要模型训练
        self.master = "local[*]"       #默认：本地所有core执行
        self.enable_hive = False        #默认：不支持hive操作
        #加载参数配置类
        self.app_name = app_name
        # 图片存储路径
        self.image_path = "D:/data/image/"
        # 汉字字体，优先使用楷体，找不到则使用黑体
        plt.rcParams['font.sans-serif'] = ['Kaitt', 'SimHei']
        # 正常显示负号
        plt.rcParams['axes.unicode_minus'] = False


    def getModelData(self):
        """
        获取建模数据：输出训练集、测试集
        :return:
        """
        train_data = None
        test_data = None
        return train_data, test_data

    def initModel(self):
        """
        初始化模型
        """
        pass

    def buildModel(self, train_data):
        """
        训练模型
        """
        pass

    def buildModelAppend(self, train_data):
        """
        训练增量模型
        """
        pass

    def loadModel(self):
        """
        加载模型
        """
        pass

    def saveModel(self):
        """
        保存模型
        """
        pass

    def predict(self, *args, **kwargs):
        """预测"""
        pass

    def evalModel(self, train_data, test_data):
        """
        评估模型
        """
        pass

    def execute(self):
        """
        执行模型
        """
        #读取数据
        train_data, test_data = self.getModelData()
        #训练模型
        if self.IS_APPEND_MODEL:
            self.initModel()
            print('Building Model ...')
            try:
                self.buildModelAppend(train_data)
            except ImportError:
                self.buildModelAppend(train_data)
        else:
            ##是否需要加载模型
            if self.IS_LOAD_MODEL:
                print('Loading model ...')
                self.loadModel()
            else:
                print('Initializing model ...')
                self.initModel()
                print('Building Model ...')
                if self.IS_TRAIN_MODEL:
                    try:
                        self.buildModel(train_data)
                    except ImportError:
                        self.buildModel(train_data)

        # 评估模型
        if self.IS_MODEL_EVAL:
            print('Evaluating Model ...')
            self.evalModel(train_data, test_data)

        # 保存模型
        if self.IS_SAVE_MODEL:
            print('Saving model ...')
            self.saveModel()