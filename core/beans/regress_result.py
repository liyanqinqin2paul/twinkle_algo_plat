# _*_ coding: utf-8 _*_
# @Date : 2023/3/13 17:21
# @Author : Paul
# @File : cluster_result.py
# @Description : 分类结果存储对象

class RegressionResult:

    # 模型唯一id
    model_id = None
    # 算法名字
    algo_name = None
    # 参数
    param = None
    # app名称
    app_name = None
    # 数据摘要概要
    info = None
    # 统计学描述
    describ = None
    # 模型结果图
    pred_dis_image = None
    # 模型数据
    model_data = None
    # 评估参数
    eval_value = None
    # 状态
    status = None
    start_time = None
    end_time = None
    duration = None

    data = {}

    def __init__(self, model_id, algo_name, param, app_name, info, describ, pred_dis_image, model_data, eval_value, status, start_time, end_time, duration):
        self.model_id = model_id
        self.algo_name = algo_name
        self.param = param
        self.app_name = app_name
        self.info = info
        self.describ = describ
        self.pred_dis_image = pred_dis_image
        self.model_data = model_data
        self.eval_value = eval_value
        self.status = status
        self.start_time = start_time
        self.end_time = end_time
        self.duration = duration
        self.data["model_id"]=model_id
        self.data["algo_name"] =algo_name
        self.data["param"] =param
        self.data["app_name"] =app_name
        self.data["info"] =info
        self.data["describ"] = describ
        self.data["pred_dis_image"] =pred_dis_image
        self.data["model_data"] =model_data
        self.data["eval_value"] =eval_value
        self.data["status"] =status
        self.data["start_time"] =start_time
        self.data["end_time"] =end_time
        self.data["duration"] =duration

