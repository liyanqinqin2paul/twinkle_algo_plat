# _*_ coding: utf-8 _*_
# @Date : 2023/3/16 14:46
# @Author : Paul
# @File : preview_data_result.py
# @Description :

class PreviewDataResult:

    # 模型唯一id
    model_id = None
    # 算法名字
    algo_name = None
    # 参数
    param = None
    # app名称
    app_name = None
    # 数据摘要概要
    info = None
    # 统计学描述
    describ = None
    train_cols = None
    # 模型数据
    model_data = None
    start_time = None
    end_time = None
    duration = None
    data = {}

    def __init__(self, model_id, algo_name, param, app_name, info, describ, train_cols, model_data, start_time, end_time, duration):
        self.model_id = model_id
        self.algo_name = algo_name
        self.param = param
        self.app_name = app_name
        self.info = info
        self.describ = describ
        self.train_cols = train_cols
        self.model_data = model_data
        self.start_time = start_time
        self.end_time = end_time
        self.duration = duration

        self.data["model_id"]=model_id
        self.data["algo_name"] =algo_name
        self.data["param"] =param
        self.data["app_name"] =app_name
        self.data["info"] =info
        self.data["describ"] = describ
        self.data["train_cols"] = train_cols
        self.data["model_data"] = model_data
        self.data["start_time"] = start_time
        self.data["end_time"] = end_time
        self.data["duration"] = duration

