# _*_ coding: utf-8 _*_
# @Date : 2023/3/29 16:20
# @Author : Paul
# @File : param_train_result.py
# @Description : 超参数训练结果集

class ParamTrainResult:
    # 模型唯一id
    model_id = None
    # 算法名字
    algo_name = None
    # 参数
    param = None
    # app名称
    app_name = None
    # 预测分布趋势图片
    pred_dis_image = None
    # 状态
    status = None
    start_time = None
    end_time = None
    duration = None

    data = {}

    def __init__(self, model_id, algo_name, param, app_name,pred_dis_image,status, start_time, end_time, duration):
        self.model_id = model_id
        self.algo_name = algo_name
        self.param = param
        self.app_name = app_name
        self.pred_dis_image = pred_dis_image
        self.status = status
        self.start_time = start_time
        self.end_time = end_time
        self.duration = duration
        self.data["model_id"] = model_id
        self.data["algo_name"] = algo_name
        self.data["param"] = param
        self.data["app_name"] = app_name
        self.data["pred_dis_image"] = pred_dis_image
        self.data["status"] = status
        self.data["start_time"] = start_time
        self.data["end_time"] = end_time
        self.data["duration"] = duration