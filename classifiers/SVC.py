# _*_ coding: utf-8 _*_
# @Date : 2023/3/24 13:42
# @Author : Paul
# @File : random_forest.py
# @Description :

from classifiers.classifier import Classifier
from core.utils.log_util import LogUtil
from core.beans.classifier_result import ClassifierResult
from sklearn.svm import SVC
from core.utils.date_util import DateUtil
from core.utils.string_utils import StringUtils
import json
import sys

class TWSVC(Classifier):

    def __init__(self,
                 app_name="svm",
                 data_source_id=None,
                 table_name=None,
                 feature_cols=None,
                 class_col=None,
                 train_size=None,
                 param=None
                 ):
        """
        初始化
        :param app_name:
        :param data_source_id:
        :param table_name:
        :param feature_cols:
        :param class_col:
        :param train_size:
        """
        super(TWSVC, self).__init__(app_name=app_name,
                                             data_source_id=data_source_id,
                                             table_name=table_name,
                                             feature_cols=feature_cols,
                                             class_col=class_col,
                                             train_size=train_size,
                                             param=param)
        self.IS_MODEL_EVAL = True  # 默认：不需要评估模型
        # 预测效果分布图
        self.tree_pred_image = "descion_tree_pred_" + app_name + "_" + DateUtil.getCurrentDateSimple()

    def initModel(self):
        """
        初始化模型
        """
        algoParam = self.param["algoParam"]
        kernel = "rbf" if StringUtils.isBlack(algoParam["kernel"]) else str(algoParam["kernel"])
        gamma = "scale" if StringUtils.isBlack(algoParam["gamma"]) else algoParam["gamma"]
        degree = 3 if StringUtils.isBlack(algoParam["degree"]) else int(algoParam["degree"])
        coef0 = 0.0 if StringUtils.isBlack(algoParam["coef0"]) else float(algoParam["coef0"])
        C = 1.0 if StringUtils.isBlack(algoParam["paramC"]) else float(algoParam["paramC"])

        self.clf = SVC(kernel=kernel,
                       gamma=gamma,
                       degree=degree,
                       coef0=coef0,
                       C=C)

    def buildModel(self, train_data):
        """
        训练模型
        """
        Xtrain = train_data[0]
        Ytrain = train_data[1]
        self.clf = self.clf.fit(Xtrain, Ytrain)

    def evalModel(self, train_data, test_data):
        """
        评估模型
        """
        Xtest = test_data[0]
        Ytest = test_data[1]
        score_ = self.clf.score(Xtest, Ytest)

        # 结束时间
        end_time = DateUtil.getCurrentDate()
        cost_second = DateUtil.diffMin(self.start_time, end_time)

        # 模型结果存入mysql
        algo_result = ClassifierResult(self.param["id"],
                                       "svc",
                                       self.param,
                                       self.app_name,
                                       self.info,
                                       self.describe,
                                       None,
                                       None,
                                       score_,
                                       "sucess",
                                       self.start_time,
                                       end_time,
                                       cost_second)
        LogUtil.saveClassifierResult(self.meta_data_source, algo_result)


if __name__ == '__main__':
    argv = sys.argv[1]
    # argv = "{\"algoParam\":{\"coef0\":\"\",\"degree\":\"\",\"gamma\":\"\",\"kernel\":\"rbf\",\"paramC\":\"\"},\"appName\":\"lr\",\"classCols\":\"Survived\",\"dataSourceId\":\"9\",\"featureCols\":\"Pclass,Sex,Age,SibSp,Parch,Fare,Embarked\",\"id\":\"1679640721133\",\"preProcessMethodList\":[{\"preProcessFeature\":\"Age\",\"preProcessMethod\":\"fillna\",\"preProcessMethodValue\":\"mean\"},{\"preProcessFeature\":\"Sex\",\"preProcessMethod\":\"transClassFeature\"},{\"preProcessFeature\":\"Embarked\",\"preProcessMethod\":\"transClassFeature\"}],\"tableName\":\"titanic\",\"trainDataRatio\":\"0.7\"}"
    param = json.loads(argv)
    app_name = param["appName"]
    data_source_id = param["dataSourceId"]
    table_name = param["tableName"]
    feature_cols = param["featureCols"]
    class_cols = param["classCols"]
    train_size = float(param["trainDataRatio"])
    class_cols_list = []
    if isinstance(class_cols, list):
        class_cols_list = class_cols
    else:
        class_cols_list.append(class_cols)
    classifier = TWSVC(app_name=app_name,
                                data_source_id=data_source_id,
                                table_name=table_name,
                                feature_cols=feature_cols,
                                class_col=class_cols_list,
                                train_size=train_size,
                                param=param)
    if "paramTrain" not in param.keys():
        classifier.execute()
    else:
        classifier.paramTrain()
