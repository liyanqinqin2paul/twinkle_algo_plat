# _*_ coding: utf-8 _*_
# @Date : 2023/3/14 18:47
# @Author : Paul
# @File : classifiers.py
# @Description :
import pandas as pd
import io
import matplotlib.pyplot as plt
from core.utils.string_utils import StringUtils
from sklearn.model_selection import train_test_split
from sklearn.impute import SimpleImputer

from core.algo.base_algo import BaseAlgo
from core.data_source.meta_data_source.meta_data_source import MetaDataSource
from core.utils.data_souce_init_utils import DataSourceInitUtil
from core.utils.date_util import DateUtil


class Classifier(BaseAlgo):

    def __init__(self,
                 app_name="clusters",
                 data_source_id=None,
                 table_name=None,
                 feature_cols=None,
                 class_col=None,
                 train_size=None,
                 param = None
                 ):
        """
        初始化类
        :param app_name:
        :param data_source_id:
        :param table_name:
        :param train_cols:
        """
        super(Classifier, self).__init__(app_name=app_name)
        self.param = param
        # 开始时间
        self.start_time = DateUtil.getCurrentDate()
        self.table_name = table_name
        self.feature_cols = feature_cols
        self.class_col = class_col
        self.train_size = train_size
        self.all_col = self.feature_cols + self.class_col
        self.clf = None
        # 数据的摘要概要
        self.info = None
        # 数据统计学估计
        self.describe = None
        # 数据二维分布图
        self.two_dim_dis_image = self.image_path + "two_dim_dis_" + app_name + "_" + DateUtil.getCurrentDateSimple() + ".png"
        # 预测效果分布图
        self.classifier_pred_image = self.image_path + "cluster_pred_" + app_name + "_" + DateUtil.getCurrentDateSimple() + ".png"
        # 获取元数据库
        self.meta_data_source = MetaDataSource()
        # 获取训练集所在的数据源
        self.data_source = DataSourceInitUtil.getDataBase(self.meta_data_source,
                                                          data_source_id)
        self.labels = None
        self.train_data_ratio = float(self.param["trainDataRatio"])

    def getModelData(self):
        """
        获取建模数据：输出训练集、测试集
        :return:
        """
        data_query_sql = "select {} from {}".format(",".join(self.all_col),
                         self.table_name)
        data = self.data_source.queryAll(data_query_sql)
        data = pd.DataFrame(data=data,
                                       columns = self.all_col)

        # 数据的简要摘要
        buf = io.StringIO()  # 创建一个StringIO，便于后续在内存中写入str
        data.info(buf=buf)  # 写入
        self.info = buf.getvalue()  # 读取

        # 统计学估计
        self.describe = data.describe()

        # 获取预处理策略值
        process_method_list_after_process = []
        self.param.get("preProcessMethodList")[0].get("preProcessFeature")
        process_method_list = self.param.get("preProcessMethodList")
        if len(process_method_list) > 0:
            for process_method in process_method_list:
                if process_method == None or process_method == "null":
                    continue
                pre_process_feature = process_method.get("preProcessFeature")
                if StringUtils.isBlack(pre_process_feature):
                    continue
                else:
                    process_method_list_after_process.append(process_method)
        self.param["preProcessMethodList"] = process_method_list_after_process
        if len(process_method_list_after_process) > 0:
            for process_method in process_method_list_after_process:
                pre_process_feature = process_method.get("preProcessFeature")
                preProcessMethod = process_method.get("preProcessMethod")
                preProcessMethodValue = process_method.get("preProcessMethodValue")

                #1.删除填充值
                if preProcessMethod == "deletena":
                    data.drop(pre_process_feature, inplace=True, axis=1)
                #2.替换缺失值
                elif preProcessMethod == "fillna":
                    if preProcessMethodValue == "mean":
                        imp_mean = SimpleImputer()
                        data[pre_process_feature] = imp_mean.fit_transform(data[pre_process_feature].values.reshape(-1,1))
                    elif preProcessMethodValue == "median":
                        imp_median = SimpleImputer(strategy="median")
                        data[pre_process_feature] = imp_median.fit_transform(data[pre_process_feature].values.reshape(-1,1))
                    elif preProcessMethodValue == "most_frequent":
                        imp_mode = SimpleImputer(strategy="most_frequent")
                        data[pre_process_feature] = imp_mode.fit_transform(data[pre_process_feature].values.reshape(-1,1))
                    elif preProcessMethodValue == "constant_0":
                        imp_0 = SimpleImputer(strategy="constant", fill_value=0)
                        data[pre_process_feature] = imp_0.fit_transform(data[pre_process_feature].values.reshape(-1,1))
                    elif preProcessMethodValue == "constant_1":
                        imp_1 = SimpleImputer(strategy="constant", fill_value=1)
                        data[pre_process_feature] = imp_1.fit_transform(data[pre_process_feature].values.reshape(-1,1))
                # 3.分类变量转换为数值变量
                elif preProcessMethod == "transClassFeature":
                    unique_value = data[pre_process_feature].unique().tolist()
                    data[pre_process_feature] = data[pre_process_feature].apply(lambda x: unique_value.index(x))
                # 4.类型转换
                elif preProcessMethod == "transType":
                    if preProcessMethodValue == "int":
                        data[pre_process_feature] = data[pre_process_feature].astype("int")
                    elif preProcessMethodValue == "float":
                        data[pre_process_feature] = data[pre_process_feature].astype("float")

        # # 处理缺失值，对缺失值较多的列进行填补，有一些特征只确实一两个值，可以采取直接删除记录的方法
        # data["Age"] = data["Age"].fillna(data["Age"].mean())
        # # 将分类变量转换为数值型变量
        # # 将二分类变量转换为数值型变量
        # # astype能够将一个pandas对象转换为某种类型，和apply(int(x))不同，astype可以将文本类转换为数字，用这个方式可以很便捷地将二分类特征转换为0~1
        # data["Sex"] = (data["Sex"] == "male").astype("int")
        # # 将三分类变量转换为数值型变量
        # self.labels = data["Embarked"].unique().tolist()
        # data["Embarked"] = data["Embarked"].apply(lambda x: self.labels.index(x))

        X = data.iloc[:, data.columns != self.class_col[0]]
        Y = data.iloc[:, data.columns == self.class_col[0]]
        # 数据无纲量化策略
        standardization = self.param["standardization"]
        if standardization == "MinMaxScaler":
            from sklearn.preprocessing import MinMaxScaler
            scaler = MinMaxScaler()
            X = scaler.fit_transform(X)
        elif standardization == "StandardScaler":
            from sklearn.preprocessing import StandardScaler
            scaler = StandardScaler()
            X = scaler.fit_transform(X)

        Xtrain, Xtest, Ytrain, Ytest = train_test_split(X, Y, train_size=self.train_data_ratio)
        return [Xtrain, Ytrain], [Xtest, Ytest]


if __name__ == '__main__':
    cluster = Classifier(app_name="cluster_demo",
                      data_source_id=9,
                      table_name="titanic",
                         feature_cols=["Survived", "Pclass", "Sex", "Age", "Cabin"],
                      class_col=["Embarked"],
                      train_size=0.7)
    cluster.getModelData()