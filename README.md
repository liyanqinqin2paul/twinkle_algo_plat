## 关于晓烁算法平台--算法端
- 晓烁算法平台理念，平台化、图形化、服务化，及围绕“平台化”、“图形化”、“服务化”，聚焦机器学习，赋能行业生态，让数据产生价值。<br>
- 晓烁算法平台，封装了大量技术开发包、技术应用组件、机器学习算法包，提供了图形化的软件交互界面，功能模块直观易懂，拖拽式操作能快速搭建数据分析方案。<br>
- 晓烁算法平台，提供了数据预览、数据预处理、数据无量钢化、数据集划分、机器学习算法建模、超参数学习和调优、算法结果可视化等机器学习全过程。
数据预处理包含了了删除缺失值、填充缺失值（均值、中位数、众数、常数）、类型转换、分类变量转换为数值型变量等策略。
数据无量钢化包含了数据归一化、数据标准化等策略。
数据集划分包含了按照比例划分训练集和测试集的策略。
机器学习算法建模包含了常用的聚类算法、分类算法、回归算法及结果查看。
超参数学习和调优提供了算法主要参数调整和参数调优效果可视化功能。
算法结果可视化提供了机器学习过程可视化和建模结果图像化的效果。<br>
## 联系方式
- QQ群: 653385927（备注来源：晓烁算法）建表sql文件和建模数据都在群里
## 项目地址
###### web端
[twinkle_algo_plat_web](https://gitee.com/liyanqinqin2paul/twinkle_algo_plat_web)
###### 算法端
[twinkle_algo_plat](https://gitee.com/liyanqinqin2paul/twinkle_algo_plat)

## 架构图
###### 机器学习建模流程图
![](./doc/images/机器学习建模流程图.jpg)


## 主要技术栈
#### WEB端技术栈
- 开发框架：Spring Boot 2.3

#### 算法端技术栈
- 开发框架：Python3.7.9 + Pycharm
- Scikit-learn 1.0.2
- Graphviz 0.8.4 (没有画不出决策树哦)
- Numpy 1.18.5, Pandas 1.3.5, Matplotlib 3.5.3, SciPy 1.4.1
## 模块说明
```
算法平台
twinkle_algo_plat
├── core -- 基础类
├── classifiers -- 分类算法模块
├    ├── classifier.py -- 分类算法基础类
├    ├── decision_tree.py -- 决策树算法类
├    ├── logistic_regression.py -- 逻辑回归算法类
├    ├── naive_bayes.py -- 贝叶斯分类算法类
├    ├── random_forest.py -- 随机森林算法类
├    ├── SVC.py -- 支持向量机分类算法类
├── clusters -- 聚类算法模块
├    ├── cluster.py -- 聚类算法基础类
├    ├── kmeans.py -- K-均值聚类算法类
└── pre_data_process -- 预览数据模块
├    ├── preview_data.py -- 预览数据算法类
├── regressions -- 回归算法模块
├    ├── regression.py -- 回归算法基础类
├    ├── linear_regression.py -- 线性回归算法类
├    ├── XGB_regressor.py -- XGBoost回归算法类
```
## 功能说明
 WEB端平台
 
 twinkle_algo_plat_web
- 聚类算法模块
   - kmeans算法：数据预览、kmeans超参数学习（参数训练）、kmeans算法建模、kmeans建模结果查看
   - 聚类算法建模记录：聚类算法历史建模记录查看、聚类建模结果查看
   
- 分类算法模块
    - 决策树算法：数据预览、决策树超参数学习（参数训练）、决策树算法建模、决策树建模结果查看
    - 逻辑回归算法：数据预览、逻辑回归超参数学习（参数训练）、逻辑回归算法建模、逻辑回归建模结果查看
    - 随机森林算法：数据预览、随机森林超参数学习（参数训练）、随机森林算法建模、随机森林建模结果查看
    - SVC算法：数据预览、SVC超参数学习（参数训练）、SVC算法建模、SVC建模结果查看
    - 朴素贝叶斯算法：数据预览、朴素贝叶斯超参数学习（参数训练）、朴素贝叶斯算法建模、朴素贝叶斯建模结果查看
    - 分类算法建模记录：分类算法历史建模记录查看、分类建模结果查看
    
- 回归算法模块
    - 线性回归算法：数据预览、线性回归超参数学习（参数训练）、线性回归算法建模、线性回归算法建模结果查看
    - XGB算法：数据预览、XGB超参数学习（参数训练）、XGB算法建模、XGB算法建模结果查看
    - 回归算法建模记录：回归算法历史建模记录查看、回归建模结果查看

## 部署文档
[晓烁算法平台web端部署文档](https://gitee.com/liyanqinqin2paul/twinkle_algo_plat_web/blob/master/doc/%E6%99%93%E7%83%81%E7%AE%97%E6%B3%95%E5%B9%B3%E5%8F%B0web%E7%AB%AF%E9%83%A8%E7%BD%B2%E6%96%87%E6%A1%A3.md)
<br>[晓烁算法平台算法端部署文档](https://gitee.com/liyanqinqin2paul/twinkle_algo_plat_web/blob/master/doc/%E6%99%93%E7%83%81%E7%AE%97%E6%B3%95%E5%B9%B3%E5%8F%B0%E7%AE%97%E6%B3%95%E7%AB%AF%E9%83%A8%E7%BD%B2%E6%96%87%E6%A1%A3.md)

## 系统截图
###### 首页
![](./doc/images/系统首页_1.jpeg)
###### 预览数据
![](./doc/images/数据预览1.jpg)

![](./doc/images/数据预览2.jpg)
###### 数据建模
![](./doc/images/决策树_数据建模1.jpg)

![](./doc/images/决策树_数据建模结果.jpg)
###### 超参数学习（参数训练）
![](./doc/images/决策树_超参数学习1.jpg)

![](./doc/images/决策树_超参数学习2.jpg)

## 数据建模教程
### 聚类算法建模
[kmeans算法](https://gitee.com/liyanqinqin2paul/twinkle_algo_plat_web/blob/master/doc/kmeans%E7%AE%97%E6%B3%95.md)
<br>[聚类算法建模记录查看](https://gitee.com/liyanqinqin2paul/twinkle_algo_plat_web/blob/master/doc/%E8%81%9A%E7%B1%BB%E7%AE%97%E6%B3%95%E5%BB%BA%E6%A8%A1%E8%AE%B0%E5%BD%95.md)

### 分类算法
[决策树算法](https://gitee.com/liyanqinqin2paul/twinkle_algo_plat_web/blob/master/doc/%E5%86%B3%E7%AD%96%E6%A0%91%E7%AE%97%E6%B3%95.md)
<br>[逻辑回归算法](https://gitee.com/liyanqinqin2paul/twinkle_algo_plat_web/blob/master/doc/%E9%80%BB%E8%BE%91%E5%9B%9E%E5%BD%92%E7%AE%97%E6%B3%95.md)
<br>[朴素贝叶斯算法](https://gitee.com/liyanqinqin2paul/twinkle_algo_plat_web/blob/master/doc/%E6%9C%B4%E7%B4%A0%E8%B4%9D%E5%8F%B6%E6%96%AF%E7%AE%97%E6%B3%95.md)
<br>[支持向量机算法](https://gitee.com/liyanqinqin2paul/twinkle_algo_plat_web/blob/master/doc/SVC%E7%AE%97%E6%B3%95.md)
<br>[随机森林算法](https://gitee.com/liyanqinqin2paul/twinkle_algo_plat_web/blob/master/doc/%E9%9A%8F%E6%9C%BA%E6%A3%AE%E6%9E%97%E7%AE%97%E6%B3%95.md)
<br>[分类算法建模记录查看](https://gitee.com/liyanqinqin2paul/twinkle_algo_plat_web/blob/master/doc/%E5%88%86%E7%B1%BB%E7%AE%97%E6%B3%95%E5%BB%BA%E6%A8%A1%E8%AE%B0%E5%BD%95.md)

### 回归算法
[线性回归算法](https://gitee.com/liyanqinqin2paul/twinkle_algo_plat_web/blob/master/doc/%E7%BA%BF%E6%80%A7%E5%9B%9E%E5%BD%92%E7%AE%97%E6%B3%95.md)
<br>[XGB回归算法](https://gitee.com/liyanqinqin2paul/twinkle_algo_plat_web/blob/master/doc/XGB%E5%9B%9E%E5%BD%92%E7%AE%97%E6%B3%95.md)
<br>[回归算法建模记录查看](https://gitee.com/liyanqinqin2paul/twinkle_algo_plat_web/blob/master/doc/%E5%9B%9E%E5%BD%92%E7%AE%97%E6%B3%95%E5%BB%BA%E6%A8%A1%E8%AE%B0%E5%BD%95.md)