 ## 晓烁算法平台web端部署文档
 ### 开发工具
 
 - Java：1.8
 - Idea
 - Mysql：5.7.41
 
 ### 修改配置文件
 
 - 修改application.yml<br>
第6行：    url: jdbc:mysql://127.0.0.1:3306/twinkle?serverTimezone=GMT%2B8&useUnicode=true&characterEncoding=UTF-8<br>
-----修改为自己mysql的连接地址和数据库名称<br>
第7行：    username: root<br>
-----修改为自己mysql的用户名<br>
第8行    password: root<br>
-----修改为自己mysql的密码<br>
第44行     path: D:/data/image/<br>
-----修改为自己服务器的图片存储路径<br>
第46行    path: D:/pycharm/twinkle_algo_plat/<br>
-----修改为自己服务器上twinkle_algo_plat项目的路径

### 启动方式
#### 1.Idea启动方式:
![](./images/晓烁算法平台web端启动方式.jpg)

#### 2.命令行启动方式:
###### mvn clean install
###### java -jar target/twinkle_algo_plat_web.jar

