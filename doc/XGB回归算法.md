## XGB算法建模

###### 预览数据
![](./images/XGB_数据预览1.jpg)

![](./images/XGB_数据预览2.jpg)
###### 数据建模
![](./images/XGB_数据建模1.jpg)

![](./images/XGB_数据建模结果1.jpg)
###### 超参数学习（参数训练）
![](./images/XGB_参数训练1.jpg)

![](./images/XGB_参数训练2.jpg)

![](./images/XGB_参数训练3.jpg)